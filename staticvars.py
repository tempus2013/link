from syjon.settings import STATIC_URL

ICON_ROOT=STATIC_URL+'link/images/umcs/'

departmenticon={
                2: ('wmfi_22.png', '0a5896'),
                3: ('wmfi_22.png', '0a5896'),
                4: ('wmfi_22.png', '0a5896'),
                5: ('wmfi_22.png', '0a5896'),
                6: ('wmfi_22.png', '0a5896'),
                7: ('wmfi_22.png', '0a5896'),
                29:('wmfi_22.png', '0a5896'),
                45:('wmfi_22.png', '0a5896'),
                46:('wmfi_22.png', '0a5896'),
                48:('wmfi_22.png', '0a5896'),
                49:('wmfi_22.png', '0a5896'),
                50:('wmfi_22.png', '0a5896'),
                52:('wmfi_22.png', '0a5896'),
                53:('wmfi_22.png', '0a5896'),
                54:('wmfi_22.png', '0a5896'),
                55:('wmfi_22.png', '0a5896'),
                56:('wmfi_22.png', '0a5896'),
                57:('wmfi_22.png', '0a5896'),
                58:('wmfi_22.png', '0a5896'),
                59:('wmfi_22.png', '0a5896'),
                34:('wmfi_22.png', '0a5896'),
                36:('wmfi_22.png', '0a5896'),
                37:('wmfi_22.png', '0a5896'),
                38:('wmfi_22.png', '0a5896'),
                39:('wmfi_22.png', '0a5896'),
                40:('wmfi_22.png', '0a5896'),
                41:('wmfi_22.png', '0a5896'),
                42:('wmfi_22.png', '0a5896'),
                43:('wmfi_22.png', '0a5896'),
                44:('wmfi_22.png', '0a5896'),
                27:('wnozigp_22.png','')
}

def icon(id):
    if departmenticon.has_key(id):
        return ICON_ROOT+departmenticon[id][0],departmenticon[id][1]
    else:
        return ICON_ROOT+'wnoname_22.png',""