import datetime 

class Activity:
    def __init__(self, id, details, group, groupCount, teachers, students, parentType, parentId):
        self.timegroupId=id
        self.activitygroupId=str(details[0])
        self.subjectId=str(details[1])
        self.subjectName=details[2]
        self.typeId=str(details[3])
        self.typeName=details[4]
        self.typeShortcut=details[5]
        if details[7]!=None:
            self.startTime=details[6]
            self.weekday=details[7]
            self.workLength=details[8]
            self.breakLength=details[9]
        else:
            self.weekday=0
            self.startTime=datetime.time(0,0,0)
            self.workLength=datetime.timedelta(0,0,0)
            self.breakLength=datetime.timedelta(0,0,0)

        self.dateclusterId=str(details[10])
        if details[12]!=None:
            self.roomId=details[12]
            self.roomName=details[13]
        else:
            self.roomId=0
            self.roomName=""

        self.group=group
        self.groupCount=groupCount
        self.students=students
        self.teachers=teachers
        
        self.parentType=parentType
        self.parentId=parentId
        
    def setColor(self, background, outline):
        self.backgroundColor=background
        self.outlineColor=outline  
    
           
    def studentsList(self):
        result=[]
        for student in self.students:
            if self.parentId!=str(student[0]):
                resultEntry={}
                resultEntry['id']=student[0]
                resultEntry['name']=student[1]
                result.append(resultEntry)
        return result
        '''
        result=[]
        for student in self.students:
            for entry in self.entryList:
                if entry['type']==1 and entry['id']==student:
                    if self.parentId!=str(student):
                        resultEntry={}
                        resultEntry['id']=entry['id']
                        resultEntry['name']=entry['name']+'\n';
                        result.append(resultEntry)
        return result
        '''
    
    def teachersList(self):
        result=[]
        for teacher in self.teachers:
            if self.parentId!=str(teacher[0]):
                resultEntry={}
                resultEntry['id']=teacher[0]
                resultEntry['name']=teacher[1]
                result.append(resultEntry)
        return result
        '''
        
        result=[]
        for teacher in self.teachers:
            for entry in self.entryList:
                if entry['type']==2 and entry['id']==teacher:
                    if self.parentId!=str(teacher):
                        resultEntry={}
                        resultEntry['id']=entry['id']
                        resultEntry['name']=entry['name']+'\n';
                        result.append(resultEntry)
        return result;
        '''
      
            
            