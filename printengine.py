# -*- coding: utf-8 -*-
import os
import codecs
from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import connection, transaction
from django.utils.safestring import mark_safe
from django.http import HttpResponse
from django.core.files.temp import NamedTemporaryFile


def page_pdf(page, file_name, request):
    # Wygenerowanie pliku HTML
    # page = render_to_string(TEMPLATE_ROOT + template_name, {'syllabus': syllabus}, context_instance=RequestContext(request))
    
    # Stworzenie nazw plikw tymczasowych
    created_name = NamedTemporaryFile(delete=True)
    filename_HTML = created_name.name+'.html'
    filename_PDF = created_name.name+'.pdf'
    
    # Zapisanie pliku HTML
    output_file = codecs.open(filename_HTML, 'w','utf-8')
    output_file.write(page)
    output_file.close()
       
    # Stworzenie pliku PDF
    # xvfb-run
    command = 'xvfb-run wkhtmltopdf -B 5 -L 4 -R 8 -T 5 -O landscape %s %s' % (filename_HTML, filename_PDF)
    os.system(command)
    
    # Wczytanie pliku PDF
    #try:
    pdf_data = open(created_name.name + '.pdf', 'rb').read()
    #except:
    #    raise Exception('server overload')
        #messages.error(request, 'W tej chwili serwer jest przeciążony. Nie udało się wygenerować pliku PDF. Spróbuj ponowanie za chwilę.')
        #return redirect(reverse('apps.metacortex.views.syllabus_my.show'))

    # Usunięcie plików tymczasowych
    created_name.close()
    os.system('rm '+filename_HTML)
    os.system('rm '+filename_PDF)
    
    response = HttpResponse(pdf_data, mimetype='application/pdf')
    response['Content-Disposition'] = 'filename=%s.pdf' % file_name
    return response