# -*- coding: utf-8 -*-

import psycopg2, psycopg2.extras
import time
import staticvars
import colorsys

from django.shortcuts import render_to_response, get_object_or_404
from django.template import RequestContext
from django.http import HttpResponseRedirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from django.db import connection, transaction
from django.utils.safestring import mark_safe
from django.http import HttpResponse
from django.template.loader import render_to_string

from string import replace
from syjon.settings import PROJECT_PATH
from calendar import weekday
from printengine import page_pdf 
from apps.syjon.lib.functions import utf2ascii

from apps.link.activity import Activity
from apps.link.forms import SampleForm

from apps.trainman.models import Teacher


TEMPLATE_ROOT = 'link/'
QUERY_ROOT = PROJECT_PATH+'/../apps/link/query/'


def index(request, id='1'):
    request.session['weekday']=0

    cursor=dbconnect(request)
    drawValues=drawdata(request)

#    try:
#        entryList = request.session['entry_list']
#    except:
    request.session['unit']=id
    
    unitFile = open(QUERY_ROOT + 'getDepartmentNameForId', 'r')
    unitQuery = unitFile.read()
    unitQuery = replace(unitQuery, '%1', id)
    cursor.execute(unitQuery)
    request.session['unit_id']=str(id)   
    request.session['unit_name']=cursor.fetchall()[0][0]
    
#    entryList = prepareEntryList(request,cursor)
#    request.session['entry_list']=entryList
    request.session['lastfilter']=""
    request.session['colorify']={'subjects': False, 'groups': []}
    lastFilter={'name': "", 'type': "4" }
    entrytable(request, lastFilter['name'], lastFilter['type'])
    filteredList=mark_safe(plainentrytable(request, lastFilter['name'], lastFilter['type']))#request.session['filteredlist']
    
    #request.session.set_expiry(30);
    
    
    kwargs = {'hLines': drawValues['hLines'],
              'vLines': drawValues['vLines'],
              'boxwidth': str(drawValues['boxWidth']),
              'filteredlist': filteredList,
              'lastfilter': filterformatted(request),
              'chosenunit': request.session['unit_name'],
              'weekday': request.session['weekday']
    }
    return render_to_response(TEMPLATE_ROOT + 'base.html', kwargs, context_instance=RequestContext(request))

def grid(request, type, id):
    kwargs=gridwebbase(request, type, id)
    print request.session
    return render_to_response(TEMPLATE_ROOT + 'base.html', kwargs, context_instance=RequestContext(request))

    
def printgrid(request):
    kwargs=gridwebbase(request, request.session['parent_type'], request.session['parent_id'], True)
    kwargs['printmode']=True
    page=render_to_string(TEMPLATE_ROOT + 'print.html', kwargs, context_instance=RequestContext(request))
    return page_pdf(page, u'plan_'+utf2ascii(replace(kwargs['chosenname'], ' ', '_')), request)
    
   

def gridbase(request, type, id, printmode=False):
    cursor=dbconnect(request)
    
    request.session['parent_type']=type;
    request.session['parent_id']=id;
    
    
#    entryList = request.session['entry_list']
    

    
    if type=='1':
        timegroupFile=open(QUERY_ROOT + 'getActivityTimegroupIdForClassgroup', 'r')
    elif type=='2':
        timegroupFile=open(QUERY_ROOT + 'getActivityTimegroupIdForTeacher', 'r')
    elif type=='3':
        timegroupFile=open(QUERY_ROOT + 'getActivityTimegroupIdForRoom', 'r')

    semesterQuery="SELECT id FROM get_current_semester()";
    cursor.execute(semesterQuery);
    semester=cursor.fetchall()[0][0]

    timegroupQuery=timegroupFile.read()
    timegroupQuery=replace(timegroupQuery, '%1', id);
    timegroupQuery=replace(timegroupQuery, '%2', str(semester)); 
    
    cursor.execute(timegroupQuery)
    timegroupIds = cursor.fetchall()
    
    templateDetailsFile=open(QUERY_ROOT + 'getActivityTimegroupDetailsForId', 'r')
    templateDetailsQuery=templateDetailsFile.read()
    
    templateStudentsFile=open(QUERY_ROOT + 'getClassgroupsForActivityGroupId', 'r')
    templateStudentsQuery=templateStudentsFile.read()
    
    templateTeachersFile=open(QUERY_ROOT + 'getTeachersForActivityGroupId', 'r')
    templateTeachersQuery=templateTeachersFile.read()
    
    templateCountFile=open(QUERY_ROOT + 'getActivityGroupCountForSubjectIdClassgroupId', 'r')
    templateCountQuery=templateCountFile.read()
    
    activities=[]
    
    subjects=set()
    subjects_data=set()
    rooms_data=set()
        
    for timegroupId in timegroupIds:
        detailsQuery=replace(templateDetailsQuery,  '%1', str(timegroupId[0]))
        #detailsQuery=replace(detailsQuery,  '%2', str(semester))
        cursor.execute(detailsQuery)
        detailsList=cursor.fetchall()
        if len(detailsList)!=0:
            details=detailsList[0]
        else:
            continue
        subjects.add(details[2])
        subjects_data.add((details[1],details[2],details[3],details[4],details[5]))
        rooms_data.add((details[12],details[13]))
        studentsQuery=replace(templateStudentsQuery,'%1', str(details[0]))
        cursor.execute(studentsQuery)
        group=1
        students=[]
        for student in cursor.fetchall():
            students.append((student[0],student[2]))
            if type=='1' and str(student[0])==id:
                group=student[1]

        teachersQuery=replace(templateTeachersQuery,'%1', str(details[0]))
        cursor.execute(teachersQuery)
        teachers=[(teacher[0],teacher[1]) for teacher in cursor.fetchall()]

        groupCount=1
        if type=='1':
            countQuery=replace(templateCountQuery,'%1',str(details[1]))
            countQuery=replace(countQuery,'%2',str(id))
            cursor.execute(countQuery)    
            groupCount=cursor.fetchall()[0][0]
        activities.append(Activity(timegroupId[0],details,group,groupCount,teachers, students, request.session['parent_type'],request.session['parent_id']))


    #kolorowanie
    colorCount=len(subjects)
    if(colorCount>0):
        colorStep=1./colorCount
        colors={}
        subjects=list(subjects)
        for i in range(colorCount):
            colors[subjects[i]]=colorStep*i
            
        for activity in activities:
            color=colors[activity.subjectName]
            
            activity.backgroundColor=colorsys.hsv_to_rgb(color, 0.1, 0.9)
            activity.outlineColor=colorsys.hsv_to_rgb(color, 0.3, 0.9)
    #print colors
    return (activities,subjects_data,rooms_data)


def gridwebbase(request, type, id, printmode=False):
    
    cursor=dbconnect(request)
    drawValues=drawdata(request)

    lastFilter= request.session['lastfilter']
    #print ">>>"+lastFilter['name']+"\t"+str(lastFilter['type']);
    #entrytable(request,lastFilter['name'], lastFilter['type'])
    filteredList=mark_safe(plainentrytable(request, lastFilter['name'], lastFilter['type']))

    #request.session['activities']=activities
    (activities,_,_)=gridbase(request,type,id,printmode)
    activitiesgrid=mark_safe(plainActivitiesGrid(request, activities, printmode))
    #activitiesgrid=request.session['activitiesgrid']


         
    chosenName=""
    records=[]
    if type=='1':
        cursor.execute("SELECT get_classgroup(%s)" % id)
        records=cursor.fetchall()  
    if type=='2':
        cursor.execute("SELECT get_teacher(%s)" % id)
        records=cursor.fetchall()
    if type=='3':
        cursor.execute("SELECT get_room(%s)" % id)
        records=cursor.fetchall()

    
    if records!=[]:
        chosenName=records[0][0]
        
    '''
    for entry in entryList:
        if str(entry['type'])==type and str(entry['id'])==id:
            chosenName=entry['name']
    '''
       
    kwargs = {'hLines': drawValues['hLines'],
              'vLines': drawValues['vLines'],
              'boxwidth': str(drawValues['boxWidth']),
              'filteredlist': filteredList,
              'lastfilter': filterformatted(request),
              'lastfiltertype': request.session['lastfilter']['type'],
              'chosenunit': request.session['unit_name'],
              'chosenname': chosenName,
              'activities': activitiesgrid, 
              'weekday': request.session['weekday'],
              'nonblank' : True
    }
    return kwargs
    

def dbconnect(request):
    db_login = "host='212.182.24.109' "
    db_login += "port='5432' "
    db_login += "dbname='syjon_schedule' "
    db_login += "user='syjon' "
    db_login += "password='b37805b582' "
    
    try:
        connection = psycopg2.connect(db_login)
    except:
        messages.error(request, 'Cannot establish connection do plan database.')
        return
    
    cursor=connection.cursor()
    cursor.execute("SELECT dblink_connect('syjon','hostaddr=212.182.24.109 port=5432 dbname=syjon user=syjon password=b37805b582')");
    connection.commit() 
    return cursor

def drawdata(request):
    boxWidth=100./splits(request)

    hLines=[]
    hour=8;
    for line in range(0,13):
        hLines.append({'top': str(line*100./13.), 'label': str(hour)+":00"})
        hour+=1
    hLines.append({'top': (100), 'label': ""})
        
    vLines=[]
    for line in range(0,int(splits(request))+1):
        vLines.append(str(line*100./splits(request)))
        
    return({'boxWidth':boxWidth, 'hLines':hLines, 'vLines':vLines})

def prepareEntryList(request, cursor):
        entryList = []
        
        studentsFile = open(QUERY_ROOT + 'getStudents', 'r')
        studentsQuery = studentsFile.read()
        studentsQuery = replace(studentsQuery, '%1', '2')
        studentsQuery = replace(studentsQuery, '%2', request.session['unit'])
    
        cursor.execute(studentsQuery)
        records = cursor.fetchall()
        for record in records:
            entry = {}
            if record[7] == True:
                entry['name'] = str(record[3]) + ' ' + record[2] + ' ' + record[5] + ' ' + record[4]
                if record[9] !=None:
                    entry['name']=entry['name']+", "+record[8]
            else:
                entry['name'] = record[2]
            entry['type'] = record[0]
            entry['id'] = record[1]
            entry['searchname'] = (entry['name']).split(' ')
            #print entry['name']+"\t"+(entry['name']).split(' ')
            entry['department_check'] = record[6]

            entryList.append(entry)
            
        teachersFile = open(QUERY_ROOT + 'getTeachers', 'r')
        teachersQuery = teachersFile.read()
        teachersQuery = replace(teachersQuery, '%1', request.session['unit'])

        cursor.execute(teachersQuery)
        records = cursor.fetchall()
        
        for record in records:
            entry = {}
            visibleName = ""
            if record[5] != None:
                visibleName += record[5] + ' '
            if record[3] != None:
                visibleName += record[3] + ' '
            if record[4] != None:
                visibleName += record[4]
            visibleName += record[2]
            entry['name'] = visibleName
            entry['type'] = record[0]
            entry['id'] = record[1]
            entry['searchname'] = [record[2]]
            entry['department_check'] = record[6]
            
            entryList.append(entry)
            
        roomsFile = open(QUERY_ROOT + 'getRooms', 'r')
        roomsQuery = roomsFile.read()
        roomsQuery = replace(roomsQuery, '%1', request.session['unit'])
        
        cursor.execute(roomsQuery)
        records = cursor.fetchall()
        
        for record in records:
            entry = {}
            entry['name'] = record[2]
            entry['type'] = record[0]
            entry['id'] = record[1]
            entry['searchname'] = [record[2]]
            entry['department_check'] = record[3]

            entryList.append(entry)
            
        return entryList
    
def filterformatted(request):
    return replace(request.session['lastfilter']['name'],'_',' ')
    
def plainentrytable(request, filter, filtertype):
    if filter=="0":
        filter=request.session['lastfilter']['name']
    if filtertype=="0":
        filtertype=request.session['lastfilter']['type']
    request.session['lastfilter']={'name': filter, 'type': filtertype}
    filter=replace(filter,'_',' ')
    
    cursor=dbconnect(request)
    
    semesterQuery="SELECT id FROM get_current_semester()";
    cursor.execute(semesterQuery);
    semester=cursor.fetchall()[0][0]
    
    result=""
    records=[]
    if filtertype=='1' or filtertype=='4':
        cursor.execute("SELECT * FROM get_students('%s',%s,%s,%s) AS (type integer, id integer, visible text, id_department integer, department text)" % (filter, filtertype, request.session['unit_id'], str(semester)))
        records += cursor.fetchall()
    if filtertype=='2' or filtertype=='4':
        cursor.execute("SELECT * FROM get_teachers('%s',%s) AS (type integer, id integer, visible text, id_department integer, department text)" % (filter, request.session['unit_id']))
        records += cursor.fetchall()
    if filtertype=='3' or filtertype=='4':
        cursor.execute("SELECT * FROM get_rooms('%s',%s) AS (type integer, id integer, visible text, id_department integer, department text)" % (filter, request.session['unit_id']))
        records += cursor.fetchall()

    
        
    for record in records:
        icon,color=staticvars.icon(int(record[3]))
        result+='<tr><td class="entrylink"><a href="%s" >%s</a></td><td class="entrydepartmenttd"><a href="%s" title="%s"><img src="%s"/></a></td><tr>' % (reverse("apps.link.views.grid", kwargs={'type':str(record[0]),'id':str(record[1])}), record[2], reverse("apps.link.views.index",kwargs={'id':str(record[3])}), record[4],icon)
    
    '''
    entryList=request.session['entry_list']
    filterParts=filter.split('_')
    maxFilterLengt=0
    for filterPart in filterParts:
        if maxFilterLengt<len(filterPart):
            maxFilterLengt=len(filterPart)
    
    for entry in entryList:
        #print filtertype+"\t"+entry['type']
        if filtertype<"4":
            if entry['type']!=int(filtertype):
                continue;
            #else:
                
        #print(entry['searchname']) 
        if maxFilterLengt<5:
            if entry['department_check']==False:
                continue
        add=True
        for filterPart in filterParts:
            addPart=False
            for searchPart in entry['searchname']:
                if (searchPart.lower()).startswith(filterPart.lower()): #TODO: zoptymalizowac
                   addPart=True
            if not addPart: 
                add=False   
                    
        if add:
            result+='<tr><td><a href="%s" class="entrylink">%s</a></td><tr>' % (reverse("apps.link.views.grid", kwargs={'type':str(entry['type']),'id':str(entry['id'])}), entry['name'])
    '''
    #request.session['filteredlist']=mark_safe(result);
    if len(result)==0:
        if request.session['unit_id']=='1':
            result='Nie odnaleziono odpowiadających wpisów.'
        else:
            result='Nie odnaleziono odpowiadających wpisów. Przełącz wyszukiwanie na <a class="notfoundlink" href="%s">cały uniwersytet</a>.' % (reverse("apps.link.views.index", kwargs={'id': '1'}))
        
    #    result=filter
    return result

def entrytable(request, filter, filtertype):
    return HttpResponse(plainentrytable(request, filter, filtertype));



def splits(request):
    wday=request.session['weekday']
    if wday>0 and wday<=7:
        return 1
    elif wday==8:
        return 5
    elif wday==9:
        return 2
    elif wday==0:
        return 7
    
def plainActivitiesGrid(request, activities, printmode=False):
    boxWidth=100./splits(request)
    hourHeight=100./13.
    
    result=""
    #activities=request.session['activities']
    
    wday=request.session['weekday']

    for activity in activities:
        if request.session['parent_type']=='1':
            groupWidth=boxWidth/float(activity.groupCount)
            groupOffset=float(activity.group-1)*groupWidth
        else:
            groupWidth=boxWidth
            groupOffset=0
            
        if wday in range(1,7):
            if activity.weekday!=wday:
                continue;
            
            weekdayOffset=0;
            weekdayFactor=0;
            
        elif wday==0:
            weekdayFactor=boxWidth
            weekdayOffset=0
            
        elif wday==8:
            weekdayFactor=boxWidth
            weekdayOffset=0
            
        elif wday==9:
            weekdayFactor=boxWidth
            weekdayOffset=5
            
            
            
        placement ="left: "+str((int(activity.weekday-1-weekdayOffset)*weekdayFactor)+groupOffset)+"%; "
        placement+="top: "+str((activity.startTime.hour+activity.startTime.minute/60.-8)*hourHeight)+"%; "
        placement+="width: "+str(groupWidth)+"%; "
        placement+="height: "+str((activity.workLength.seconds+activity.breakLength.seconds)/3600.*hourHeight)+"%; "
        color=activity.backgroundColor
        placement+="background-color: #"+"{0:0{1}x}".format((int(color[0]*255)),2)+"{0:0{1}x}".format((int(color[1]*255)),2)+"{0:0{1}x}".format((int(color[2]*255)),2)+"; "
        activity_block="activity_block"
        activity_content="activity_content"
        if printmode:
            activity_block+='_print'
            activity_content+='_print'
        result+='<div class="%s" style="%s" >' % (activity_block, placement)
        if activity.groupCount!=1:
            color=activity.outlineColor
            color="#"+"{0:0{1}x}".format((int(color[0]*255)),2)+"{0:0{1}x}".format((int(color[1]*255)),2)+"{0:0{1}x}".format((int(color[2]*255)),2)
            style="text-shadow: -1px 0 %s, 0 1px %s, 1px 0 %s, 0 -1px %s;" % (color,color,color,color)
            result+='<div class="activity_group" style="%s" >%s</div>' % (style,str(activity.group))
        result+='<div class="%s">' % activity_content
        result+='<div class="subject_content"><a title="%s" style="cursor: default">%s</a></div>' % (activity.subjectName, activity.subjectName)
        if len(activity.teachersList())>0:
            result+='<div class="teachers_content">'
            for entry in activity.teachersList():
                result+='<div><a href="%s" title="%s" >%s</a></div>' % (reverse("apps.link.views.grid", kwargs={'type':'2','id':str(entry['id'])}),entry['name'], entry['name'])
            result+='</div>'
        if len(activity.studentsList())>0:
            result+='<div class="students_content">'
            for entry in activity.studentsList():
                result+='<div><a href="%s" title="%s">%s</a></div>' % (reverse("apps.link.views.grid", kwargs={'type':'1','id':str(entry['id'])}),entry['name'], entry['name'])
            result+='</div>'
        result+='<div class="bottom_content_containter">';
        if not (request.session['parent_type']=='3' and request.session['parent_id']==str(activity.roomId)):
            result+='<div class="room_content"><a href="%s" title="%s">%s</a></div>'     % (reverse("apps.link.views.grid", kwargs={'type':'3','id':str(activity.roomId)}),activity.roomName, activity.roomName)
        result+='<div class="type_content"><a title="%s">%s</a></div>'     % (activity.typeName,activity.typeShortcut)
        result+='</div>'
        result+='</div></div>'
    #request.session['activitiesgrid']=mark_safe(result);
    return result;

def activitiesGrid(request, activities, printmode=False):
    return HttpResponse(plainActivitiesGrid(request, activities, printmode));
    
def weekday(request, day):
    request.session['weekday']=int(day)
    return grid(request, request.session['parent_type'], request.session['parent_id'])

def plaindata(request, id):
    (activities,subjects,rooms)=gridbase(request, '1', id)
    types=set()
    teacher_ids=set()
    student_ids=set()
    for subject in subjects:
        types.add((subject[2],subject[3],subject[4]))
    for activity in activities:
        for teacher in activity.teachers:
            teacher_ids.add(teacher[0])
        for student in activity.students:
            student_ids.add((student[0],student[1]))
    teachers = Teacher.objects.filter(id__in=teacher_ids)

    kwargs = {'activities': activities, 'subjects': subjects, 'types':types, 'teachers': teachers, 'students': student_ids, 'rooms': rooms}
    return render_to_response('link/plaindata.xml', kwargs, context_instance=RequestContext(request))
