# -*- coding: utf-8 -*-

from django.conf.urls.defaults import patterns

urlpatterns = patterns(
    'apps.link.views',
    (r'^$', 'index'),
    (r'^(?P<id>\d+)$', 'index'),
    (r'^filtered/(?P<filter>\w{0,50})/(?P<filtertype>\d+)/?$', 'entrytable'),
    (r'^grid/(?P<type>\d+)/(?P<id>\d+)/?$', 'grid'),
    (r'^printgrid/$', 'printgrid'),
    (r'^weekday/(?P<day>\d+)$', 'weekday'),
    (r'^plaindata/(?P<id>\d+)$', 'plaindata'),
)
